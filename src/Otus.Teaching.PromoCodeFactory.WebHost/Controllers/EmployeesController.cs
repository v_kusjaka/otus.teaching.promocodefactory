﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавление нового сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> СreateEmployeeAsync(EmployeeResponse employeeResponse)
        {
            var names = employeeResponse.FullName.Split(' ');

            var employee = new Employee
            {
                Id = employeeResponse.Id,
                FirstName = names.FirstOrDefault(),
                LastName = names.Count() >= 2 ? names[1] : string.Empty,
                Email = employeeResponse.Email,
                AppliedPromocodesCount = employeeResponse.AppliedPromocodesCount,
                Roles = employeeResponse.Roles.Select(x => new Role
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList()
            };
            await _employeeRepository.CreateAsync(employee);
            return Ok();
        }

        /// <summary>
        /// Удаление сотрудника по Id.
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            await _employeeRepository.DeleteByIdAsync(id);
            return Ok();
        }

        /// <summary>
        /// Редактирование данных сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        public ActionResult UpdateEmployee(EmployeeResponse employeeResponse)
        {
            var names = employeeResponse.FullName.Split(' ');

            var employee = new Employee
            {
                Id = employeeResponse.Id,
                FirstName = names.FirstOrDefault(),
                LastName = names.Count() >= 2 ? names[1] : string.Empty,
                Email = employeeResponse.Email,
                AppliedPromocodesCount = employeeResponse.AppliedPromocodesCount,
                Roles = employeeResponse.Roles.Select(x => new Role
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList()
            };

            _employeeRepository.UpdateAsync(employee);
            return Ok();
        }
    }
}